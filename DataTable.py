import pandas as pd
import numpy as np
import plotly as pl
import plotly.express as px
from plotly import graph_objects as go
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input
import plotly.express as px

app = dash.Dash(__name__)
server = app.server

#---------------------------------------------------------------
# Import Data
data_location = "LANyLAS.csv"
data = pd.read_csv(data_location, sep=";")

lista_equipos = data.Team.unique()

#Mejor Acs para valor default
best_acs = list(data[data.Acs == data.Acs.max()].Ign)
best_acs[0]
# KD y ACS promedio: Info soporte para el Scatter
avg_kd = data.Kd.mean()
avg_acs = data.Acs.mean()
data.Kd=round(data.Kd,2)
#Soporte para el grafico de radar. Calculo % por columna a analizar y lista con los nombres para tomar los datos
radar_theta = ["Kd","Acs","Kills","Deaths","Assists"]
radar_pec = []
def max_relativo(dataframe,elementos):
    for i in elementos:
        maximo = dataframe[i].max()
        radar_pec.append(i+"_%")
        dataframe[i+"_%"] = dataframe.apply(lambda row: row[i]/maximo, axis = 1)

max_relativo(data,radar_theta)


df = px.data.gapminder()


app = dash.Dash(__name__)
server = app.server

app.layout = html.Div([
    html.Div([html.H1('Estadisticas Challengers',
                               style={'textAlign': 'center'})]),
    dcc.Dropdown(id="dpd_region", value=["LAS","LAN"], multi=True,
                 options=[{"label": x, "value": x} for x in data.Region.unique()]),
    dcc.Dropdown(id="dpd_equipos",value = lista_equipos, multi = True,
                 options =[{"label": x, "value": x} for x in data.Team.unique()] ),


    html.Div([

        dcc.Graph(id="graph_arriba", figure={}, clickData=None, hoverData=None,
                 config={
                      'staticPlot': False,     # True, False
                      'scrollZoom': True,      # True, False
                      'doubleClick': 'reset',  # 'reset', 'autosize' or 'reset+autosize', False
                      'showTips': False,       # True, False
                      'displayModeBar': True,  # True, False, 'hover'
                      'watermark': True,
                      # 'modeBarButtonsToAdd': ['pan2d','select2d'],
                 },
                 className="twelve columns"),

    ]),
    html.Div([
        dcc.Graph(id='graph_der', figure={}, className='six columns'),
        dcc.Graph(id="graph_izq", figure={}, className="six columns"),
    ]),

])

@app.callback(
    Output(component_id='graph_arriba', component_property='figure'),
    [Input(component_id='dpd_region', component_property='value'),
    Input(component_id = "dpd_equipos", component_property="value")]
)
def update_graph(region, equipo):
    dff = data[(data.Region.isin(region)) & (data.Team.isin(equipo))]
    fig = px.scatter(data_frame=dff, x='Kd', y='Acs', color='Team', size="Rplay", custom_data=['Ign',"Kills","FB","FB Win",],
                     hover_data=['Ign', 'Team', 'Kd', 'Acs'], text="Ign",
                  )
    fig.add_vline( x=avg_kd, y0=0,y1=300, line_width=2, line_dash="dash", line_color="red",
                  annotation_text="KD promedio total",
                  annotation_position="top right")
    fig.add_hline(x0=0,x1=1.8,y=avg_acs,line_width=2, line_dash="dash", line_color="red",
                 annotation_text="ACS promedio total",
                 annotation_position="top left")
    fig.update_xaxes(range=[0, 1.8])
    fig.update_yaxes(range=[0, 300])
    fig.update_layout(title="KD vs ACS",
                      title_font_size=20,
                      title_x= 0.5)

    return fig

@app.callback(
    Output(component_id='graph_der', component_property='figure'),
    [Input(component_id='graph_arriba', component_property='hoverData')],
    )

def update_outpost_div(input_value):

    if input_value is None:
        dff2 = data
        dff3 = dff2[dff2.Ign == best_acs[0]]
        print(dff2)
        dict_funnel = dict(
                    Bajas = [int(dff3.Kills),int(dff3.FB),int(dff3["FB Win"])],
                    Tipo= ['Frags',"Fb frags","Fb Frags wins"])
        fig2= go.Figure(

        go.Funnel(
        y = dict_funnel["Tipo"],
        x = dict_funnel["Bajas"],
        textposition = "inside",
        textinfo = "value+percent total",
        opacity = 0.65, marker = {"color": ["deepskyblue", "deepskyblue", "deepskyblue", "deepskyblue", "deepskyblue"],
        "line": {"width": [2, 2, 2, 2, 1, 1], "color": ["blue", "blue", "blue", "blue", "blue"]}},
        connector = {"line": {"color": "royalblue", "dash": "dot", "width": 3}},
        hoverinfo = "x+y+percent initial+percent previous"),
            )
        fig2.update_layout(title="Bajas totales->Primeras Bajas->Conversion de primera baja en ronda ganada",
                      title_font_size=20,
                      title_x= 0.5)

        return fig2
    else:
        dff = data
        hov_name = input_value['points'][0]['customdata'][0]

        dff2 = dff[dff.Ign == hov_name]
        kills = input_value['points'][0]['customdata'][1]
        FB = input_value['points'][0]['customdata'][2]
        FB_win= input_value['points'][0]['customdata'][3]
        dict_funnel = dict(
                    Bajas = [kills,FB,FB_win],
                    Tipo= ['Frags',"Fb frags","Fb Frags wins"])


        fig2= go.Figure(

        go.Funnel(
        y = dict_funnel["Tipo"],
        x = dict_funnel["Bajas"],
        textposition = "inside",
        textinfo = "value+percent total",
        opacity = 0.65, marker = {"color": ["deepskyblue", "deepskyblue", "deepskyblue", "deepskyblue", "deepskyblue"],
        "line": {"width": [2, 2, 2, 2, 1, 1], "color": ["blue", "blue", "blue", "blue", "blue"]}},
        connector = {"line": {"color": "royalblue", "dash": "dot", "width": 3}},
        hoverinfo = "x+y+percent initial+percent previous"),
            )
        fig2.update_layout(title="Bajas totales->Primeras Bajas->Conversion de primera baja en ronda ganada",
                      title_font_size=20,
                      title_x= 0.5)


        return fig2

@app.callback(
    Output(component_id='graph_izq', component_property='figure'),
    [Input(component_id='graph_arriba', component_property='hoverData')],
    )

def radar_chart(input_value):
    if input_value is None:
        dff2 = data
        dff3 = dff2[dff2.Ign == best_acs[0]]
        valores_per = list(dff3[radar_pec].iloc[0])
        fig3 = go.Figure()
        fig3.add_trace(go.Scatterpolar(
        r=valores_per,
        theta=radar_theta,
        fill='toself',
        name='Estadisticas'
)),
        fig3.update_layout(
        polar=dict(
        radialaxis=dict(
        visible=True),
        ),
        showlegend=False,
        title_text = 'Estadisticas relativas',
        title_font_size=18,
        title_x= 0.5)
        return fig3
    else:
        dff = data
        hov_name = input_value['points'][0]['customdata'][0]
        dff2 = dff[dff.Ign == hov_name]
        valores_per = list(dff2[radar_pec].iloc[0])
        fig3 = go.Figure()
        fig3.add_trace(go.Scatterpolar(
        r=valores_per,
        theta=radar_theta,
        fill='toself',
        name='Estadisticas'
)),
        fig3.update_layout(
        polar=dict(
        radialaxis=dict(
        visible=True),
        ),
        showlegend=False,
        title_text = 'Estadisticas relativas',
        title_font_size=18,
        title_x= 0.5)
        return fig3


if __name__ == '__main__':
    app.run_server(debug=True)
